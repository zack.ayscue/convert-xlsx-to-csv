import io
import pandas as pd
import boto3

s3_client = boto3.client("s3")


def lambda_handler(event, context):
    output = []
    records = event["Records"]
    for r in records:
        body = r["body"]
        source_bucket = body["sourceBucket"]
        source_key = body["sourceKey"]
        target_bucket = body["targetBucket"]
        target_key = body["targetKey"]
        if (
            "BA_Daily_DDA" in source_key
            or "BA_Daily_CD" in source_key
            or "BA_Daily_SAV" in source_key
            or "BA_NamesAccountRelationship" in source_key
        ):
            get_object_res = s3_client.get_object(Bucket=source_bucket, Key=source_key)
            xlsx_df = pd.read_excel(
                io.BytesIO(get_object_res["Body"].read()), skipfooter=1, dtype=str
            )
            with io.StringIO() as csv_buffer:
                xlsx_df.to_csv(csv_buffer, index=False)
                s3_client.put_object(
                    Bucket=target_bucket, Key=target_key, Body=csv_buffer.getvalue()
                )
        output.append(body)
    return output
