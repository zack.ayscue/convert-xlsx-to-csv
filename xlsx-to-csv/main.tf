terraform {
  backend "s3" {
    key    = "terraform/dw-xlsx-to-csv/statefile"
    region = "us-east-1"
  }
}

provider "aws" {
  default_tags {
    tags = {
      Service = "lob-datawarehouse-xlsx-to-csv"
    }
  }
  region = var.aws_region
}

resource "aws_iam_role" "dw_xlsx_to_csv_service_role" {
  name = "dw_xlsx_to_csv_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })

  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]

  inline_policy {
    name = "dw_xlsx_to_csv_service_s3_policy"
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Effect = "Allow",
          Action = [
            "s3:getObject",
            "s3:putObject"
          ],
          "Resource" = [
            "arn:aws:s3:::lob-depcon-639210664698/*"
          ]
        }
      ]
    })
  }
}

resource "aws_lambda_layer_version" "dw_xlsx_to_csv_service_libs" {
  filename   = "./dist/libs.zip"
  layer_name = "dw-xlsx-to-csv-service-libs"
  source_code_hash = filebase64sha256("./dist/libs.zip")

  compatible_runtimes = ["python3.8"]
}


resource "aws_lambda_function" "dw_xlsx_to_csv_service" {
  filename      = "./dist/deploy.zip"
  function_name = "dw-xlsx-to-csv-service"
  role          = aws_iam_role.dw_xlsx_to_csv_service_role.arn
  handler       = "app.lambda_handler"
  source_code_hash = filebase64sha256("./dist/deploy.zip")
  runtime = "python3.8"
  memory_size = 8192
  timeout = 900
  layers = ["${aws_lambda_layer_version.dw_xlsx_to_csv_service_libs.arn}"]
}
