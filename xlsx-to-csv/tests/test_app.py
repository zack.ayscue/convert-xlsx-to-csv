import json
from xlsx_to_csv import __version__, app


def test_version():
    assert __version__ == "0.1.0"


def test_app():
    event = json.loads(
        """
    {
        "Records": [{
            "body": {
                "sourceBucket": "lob-depcon-639210664698",
                "sourceKey": "BA_NamesAccountRelationship_1.xlsx",
                "targetBucket": "lob-depcon-639210664698",
                "targetKey": "BA_NamesAccountRelationship_1.csv"
            }
        }]
    }
    """
    )
    output = app.lambda_handler(event, {})
    assert len(output) == 1
